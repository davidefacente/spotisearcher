var spotisearcher = angular.module('spotisearcher', ["ngResource", "ngRoute"]);

spotisearcher.constant('baseUrl', "https://api.spotify.com/v1/");
//spotisearcher.constant('baseUrl', "mocks/search_artists.json");

spotisearcher.config(function($routeProvider, $locationProvider){

  $locationProvider.html5Mode(true);

  $routeProvider.when("/artist",{
    templateUrl: "html/artist.html"
  });

  $routeProvider.when("/search",{
    templateUrl: "html/search-results.html"
  });

  /*$routeProvider.otherwise({ 
    templateUrl: ""
  });*/

	console.log('config module momapix');
});



/***** CONTROLLERS ****/

spotisearcher.controller('SearchCtrl', ['$scope', '$resource', '$location', 'searchService', 'baseUrl', function($scope, $resource, $location, searchService, baseUrl){

  //  $scope.searchString = "";
	//$scope.searchResults = [{"item":"test 1"}];

	$scope.sendSearchRequest = function(searchString){	
    searchService.search(searchString).then(function(data){
      $scope.searchResults = data;
      console.log($scope.searchResults);
      $location.path('/search');
    });
	};

  $scope.showArtistDetails = function(artistId){
    searchService.fetchArtistDetails(artistId).then(function(data){
      $scope.artistDetails = data;
      console.log("fetching artist detail..");
      $location.path('/artist');
    });
  }



  searchService.search("foo").then(function(data){
      $scope.searchResults = data;
      console.log($scope.searchResults);
    });

}]);

spotisearcher.controller('SearchFormCtrl', ['$scope', function($scope){
  
}])

spotisearcher.controller('SearchResults', ['$scope', function($scope){
  
}])


/***** SERVICES *****/

spotisearcher.factory('searchService', function($http, $resource, baseUrl){

	var search = function(query){
    if(query.length) {
      return $http.get(baseUrl + "search?q=" + query + "&type=artist,album,track")
                .then(handleSuccess, handleError);
      //return $http.get(baseUrl)
      //          .then(handleSuccess, handleError);
      
    }
	}

  var fetchArtistDetails = function(artistId){
    if(artistId) {
      return $http.get(baseUrl + "artists/" + artistId)
                .then(handleSuccess, handleError);
      //return $http.get(baseUrl)
      //          .then(handleSuccess, handleError);
      
    }
  }

	function handleSuccess(response) {
    console.log(response.data);
		return response.data;

	}

	function handleError(response) {
		return response.data;
	}

  return{
    search: search,
    fetchArtistDetails: fetchArtistDetails
  };


});