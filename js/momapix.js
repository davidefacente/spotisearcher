var momapix = angular.module('momapix', ["ngResource"]);

momapix.config(function(){
	console.log('config module momapix');
});


/***** CONTROLLERS ****/

momapix.controller('SearchCtrl', ['$scope', 'searchService', function($scope, searchService){

  //  $scope.searchString = "";
	//$scope.searchResults = [{"item":"test 1"}];

	$scope.sendSearchRequest = function(searchString){
		//console.log(searchString);
		//console.log($scope.searchResults);
		/*
    searchService.search(searchString).then(function(data){
      $scope.searchResults = data;
    });
    */

    //$scope.searchResults = searchService.search2(searchString);

    
    searchService.search2(searchString).then(function(data){
      $scope.searchResults = data;
    });

	}

  console.log("$scope.searchResults: " + $scope.searchResults);

}]);

momapix.controller('SearchFormCtrl', ['$scope', function($scope){
  
}])

momapix.controller('SearchResults', ['$scope', function($scope){
  
}])


/***** SERVICES *****/

momapix.factory('searchService', function($http, $resource){

  var searchRes = $resource("http://jsonplaceholder.typicode.com/posts/:id", {id : "@id"});

	var search = function(query){

    if(query.length) {
      //return $http.get("http://jsonplaceholder.typicode.com/posts")
      //        .then(handleSuccess);

      return $http.get("https://www.googleapis.com/books/v1/volumes?q=" + query)
                .then(handleSuccess);
    }
	}

  
  var search2 = function(query){
    results = searchRes.query();
    console.log(results);
    //return results;
    return results.$promise.then(function(results){
      console.log("success");
      //console.log(results);
      console.log(results[0]);
      return results;
    }, function(results){
      console.log("error");
      return results;
    });
  }

	function handleSuccess(response) {
    console.log(response.data);
		return response.data;

	}

	function handleError(response) {
		return response.data;
	}

  return{
    search: search,
    search2: search2
  };


});